# With wsgi the default config file will be read from
# /docker/cloudpulse/cloudpulse.conf

from pecan.deploy import deploy
application = deploy('/cloudpulse/cloudpulse/config.py')
